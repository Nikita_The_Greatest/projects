#MIDGOTCHI unCLI test-version
#Dev 0.0.2

import subprocess
import os
import sys
from colorama import Fore, Back, Style
import shutil

def start_screen():
    print(Back.LIGHTBLUE_EX + Fore.LIGHTWHITE_EX + "Welcome To MIDGOTCHI v 0.0.2! (unCLI DEV)")
    func = int(input(Style.RESET_ALL + Back.LIGHTMAGENTA_EX + Fore.LIGHTWHITE_EX + """For now, we have this functions on your choose:"
                 1.R/W, remove files from current directory - also, some actions with directory :D
                 2.Detailed info about files, directories, etc.
                 3.Format files, compressing into .zip, .bzip2
                 4.Advance functions (like Mido Assistant)
                 5.EXIT
                 6.Help
                 >> """))
    if "1" in func or "2" in func:
        reda_detta()
    elif func == "3":
        forma()
    elif "4" in func or "6" in func:
        adva()
    elif func == "5":
        print("Goodbye!")
        sys.exit()
    else:
        print("Insert in the command line ONLY cifer of your choose!")
        ls1 =  os.system("cls" if os.name == "nt" else "clear")
        print(func)


def reda_detta():
    print(os.getcwd())
    subprocess.call(["python", "file_manage.py"])
def forma():
    print("TEST FORMA")
def adva():
    print("TEST ADVA")


start_screen()
