import os
import sys
import keyboard
import colorama

def conva_files():
    if os.name == "nt":
        os.system("chcp 65001")
    else:
        pass
    ls = str(os.system("dir") if os.name == "nt" else os.system("ls"))  # print directories and files with help of cmd
    open_func()

def open_func():
    global cd
    cd = input("Choose current directory or file: ")
    if os.path.isdir(cd):
        os.system(f"dir {cd}" if os.name == "nt" else os.system(f"cd {cd}"))
    elif os.path.isfile(cd):
        actions = str(input("And what do we gonna do now with file (R/W): "))
        if "W" in actions or "w" in actions:
            burner()
        elif "R" in actions or "r" in actions:
            reader()
        else:
            print("Incorrect name of function - print ONLY R or W to make some actions with file")

#def burner(): #In a development
   #with open(f"{cd}", "r+", encoding='utf-8') as file_in:
        #sometime_file = f"{file_in}Save.txt"
        #shutil.copy(f"{cd}", f"{cd}Save.txt")
        #file_in.close()
        #with open(f"{cd}", "w+", encoding='utf-8') as file_out:
            
            #file_out.write()
            #shutil.copy(f"{cd}Save.txt", f"{cd}")
            #os.remove(sometime_file)
def reader():
    os.system("cls" if os.name == "nt" else "clear")
    with open(cd, 'r', encoding='utf-8') as file:
        read = file.read()
        print(Fore.LIGHTWHITE + Back.LIGHTBLACK + (f"""{cd.center(40)}"""))
        print(f"""{read}""")
        keyboard.wait('esc')
        if keyboard.is_pressed('esc'):
            conva_files()
conva_files()
